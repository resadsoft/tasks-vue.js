module.exports = {
  head: {
    title: 'TaskList application made with Nuxt.js',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', content: 'Task List application made with Nuxt.js' }
    ],
  },
  css: [
    'todomvc-app-css/index.css',
    '~/assets/styles/todo.css'
  ],
  router: {
    linkActiveClass: 'selected'
  }
};