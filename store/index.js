import Vue from 'vue'
import Vuex from 'vuex'
import uuidv4 from 'uuid/v4'

Vue.use(Vuex);

const store = () => new Vuex.Store({
  state: {
    allTaskLists: [],
    taskList: []
  },
  getters: {
    getAllTaskLists(state) {
      return state.allTaskLists
    },
    getAllTasks(state) {
      return listId => state.taskList.filter(task => task.listId === listId)
    },
    getCompletedTasks(state) {
      return state.taskList.filter(todo => todo.completed)
    },
    getActiveTasks(state) {
      return state.taskList.filter(todo => !todo.completed)
    }
  },
  mutations: {
    // tasks list
    ADD_TASK_LIST(state, taskListName) {
      const taskListPayload = {
        id: uuidv4(),
        name: taskListName
      };
      state.allTaskLists.push(taskListPayload)
    },
    REMOVE_TASK_LIST(state, taskList) {
      const indexToRemove = state.allTaskLists.findIndex(list => list.id === taskList.id);
      state.allTaskLists.splice(indexToRemove, 1)
    },
    EDIT_TASK_LIST(state, taskList) {
      const indexToEdit = state.allTaskLists.indexOf(taskList);
      if (indexToEdit !== -1) {
        state.allTaskLists[indexToEdit] = taskList
      }
    },
    // task list items
    ADD_TASK_ITEM(state, item) {
      state.taskList.push(item)
    },
    REMOVE_TASK_ITEM(state, item) {
      const indexToRemove = state.taskList.indexOf(item);
      if (indexToRemove !== -1) {
        state.taskList.splice(indexToRemove, 1)
      }
    },
    EDIT_TASK_ITEM(state, item) {
      const indexToEdit = state.taskList.indexOf(item);
      if (indexToEdit !== -1) {
        state.taskList[indexToEdit] = item
      }
    }
  },
  actions: {
    // tasks list
    createTasksList({commit}, taskListName) {
      commit('ADD_TASK_LIST', taskListName)
    },
    removeTaskList({commit}, taskList) {
      commit('REMOVE_TASK_LIST', taskList)
    },
    editTaskList({commit}, taskList) {
      commit('EDIT_TASK_LIST', taskList)
    },
    // task list items
    addTaskItem({commit}, taskItemPayload) {
      commit('ADD_TASK_ITEM', taskItemPayload)
    },
    removeTaskItem({commit}, taskItemPayload) {
      commit('REMOVE_TASK_ITEM', taskItemPayload)
    },
    editTaskItem({commit}, taskItem) {
      commit('EDIT_TASK_ITEM', taskItem)
    }
  }
});

export default store